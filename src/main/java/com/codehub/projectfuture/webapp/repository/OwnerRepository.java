package com.codehub.projectfuture.webapp.repository;

import com.codehub.projectfuture.webapp.domain.Owner;
import com.codehub.projectfuture.webapp.model.OwnerModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface OwnerRepository extends JpaRepository<Owner, Long> {

    Owner save(Owner owner);

    void deleteById(Long id);

    void deleteByTaxRegistryNumber(Long taxRegistryNumber);

    List<Owner> findOwnerByTaxRegistryNumber(Long taxRegistryNumber);

    List<Owner> findOwnerByEmail(String email);

    Owner findByEmail(String email);

    List<Owner> findAll();

    Owner findByTaxRegistryNumber(Long taxRegistryNumber);
}

