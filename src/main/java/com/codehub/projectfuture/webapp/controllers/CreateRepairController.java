package com.codehub.projectfuture.webapp.controllers;

import com.codehub.projectfuture.webapp.domain.Repair;
import com.codehub.projectfuture.webapp.enums.FixType;
import com.codehub.projectfuture.webapp.enums.Status;
import com.codehub.projectfuture.webapp.forms.RepairForm;
import com.codehub.projectfuture.webapp.mapper.RepairFormToRepairMapper;
import com.codehub.projectfuture.webapp.service.OwnerService;
import com.codehub.projectfuture.webapp.service.RepairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

import static com.codehub.projectfuture.webapp.utils.GlobalAttributes.ERROR_MESSAGE;


@Controller
@RequestMapping("admin")
public class CreateRepairController {
    private static final String REPAIRS_FORM = "repairsForm";
    private static final String REPAIRS_STATUS = "repairsStatus";
    private static final String REPAIRS_TYPE = "repairsType";

    @Autowired
    private RepairService repairService;

    @Autowired
    private OwnerService ownerService;

    @Autowired
    private RepairFormToRepairMapper mapper;

    @GetMapping(value = "/repairs/create")
    public String repairsDynamic(Model model) {

        model.addAttribute(REPAIRS_FORM, new RepairForm());
        model.addAttribute(REPAIRS_STATUS, Status.values());
        model.addAttribute(REPAIRS_TYPE, FixType.values());
        return "pages/repairs_create";
    }

    @PostMapping(value = "/repairs/create")
    public String createRepairs(Model model,
                              @Valid @ModelAttribute(REPAIRS_FORM)
                                      RepairForm repairForm,
                              BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            //have some error handling here, perhaps add extra error messages to the model
            model.addAttribute(ERROR_MESSAGE, "errorMessage");
            return "pages/repairs_create";
        }

        Repair repair = mapper.toRepair(repairForm);
        try {
            if (!ownerService.findOwnerByTaxRegistryNumber(repair.getOwner().getTaxRegistryNumber()).isEmpty()) {
                repairService.createRepair(repair);
                return "redirect:/admin/repairs";
            } else return "wrongTaxRegistry";
        }catch (NullPointerException ex)
        {
            return "wrongTaxRegistry";
        }
    }

}

