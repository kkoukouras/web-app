package com.codehub.projectfuture.webapp.controllers;

import com.codehub.projectfuture.webapp.enums.FixType;
import com.codehub.projectfuture.webapp.enums.Status;
import com.codehub.projectfuture.webapp.model.RepairModel;
import com.codehub.projectfuture.webapp.service.RepairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("admin")
public class EditRepairController {

    private static final String REPAIR_ATTR = "repair";
    private static final String FIX_TYPE = "fixType";
    private static final String STATUS = "status";

    @Autowired
    private RepairService repairService;

    @PostMapping(value = "/repairs/{id}/delete")
    public String deleteRepair(@PathVariable Long id) {
        repairService.deleteById(id);
        return "redirect:/admin/repairs";
    }

    @GetMapping(value = "/repairs/{id}/edit")
    public String editRepair(@PathVariable Long id, Model model) {
        RepairModel repairModel = repairService.findRepair(id).get();
        model.addAttribute(REPAIR_ATTR, repairModel);
        model.addAttribute(FIX_TYPE, FixType.values());
        model.addAttribute(STATUS, Status.values());
        return "pages/repairs_edit";
    }

    @PostMapping(value = "/repairs/edit")
    public String editRepair(RepairModel repairModel) {
        repairService.updateRepair(repairModel);
        return "redirect:/admin/repairs";
    }
}

