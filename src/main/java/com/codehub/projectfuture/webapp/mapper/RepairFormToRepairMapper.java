package com.codehub.projectfuture.webapp.mapper;

import com.codehub.projectfuture.webapp.domain.Owner;
import com.codehub.projectfuture.webapp.domain.Repair;
import com.codehub.projectfuture.webapp.enums.FixType;
import com.codehub.projectfuture.webapp.enums.Status;
import com.codehub.projectfuture.webapp.forms.RepairForm;
import com.codehub.projectfuture.webapp.model.OwnerModel;
import com.codehub.projectfuture.webapp.repository.OwnerRepository;
import com.codehub.projectfuture.webapp.repository.RepairRepository;
import com.codehub.projectfuture.webapp.service.OwnerService;
import com.codehub.projectfuture.webapp.service.RepairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Optional;


@Component
public class RepairFormToRepairMapper {

    @Autowired
    OwnerService ownerService;


    public Repair toRepair(RepairForm repairForm) {
        Repair repair = new Repair();
        repair.setDate(LocalDate.parse(repairForm.getDate()));
        repair.setStatus((Status.valueOf(repairForm.getStatus())));
        repair.setCost((Double.parseDouble(repairForm.getCost())));
        repair.setFixType(FixType.valueOf(repairForm.getFixType()));
        repair.setAddress(repairForm.getAddress());

        Owner owner = ownerService.findByTaxRegistryNumber(Long.parseLong(repairForm.getOwnerTaxRegistryNumber()));

        repair.setOwner(owner);
        repair.setDescription(repairForm.getDescription());

        return repair;
    }
}
