package com.codehub.projectfuture.webapp.exception;

public class OwnersNotFoundException extends  RuntimeException{

    public OwnersNotFoundException(String msg, Throwable t) {
        super(msg, t);
    }

    public OwnersNotFoundException(String msg) {
        super(msg);
    }

}
