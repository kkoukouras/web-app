package com.codehub.projectfuture.webapp.service;

import com.codehub.projectfuture.webapp.domain.Owner;
import com.codehub.projectfuture.webapp.domain.Repair;
import com.codehub.projectfuture.webapp.enums.PropertyType;
import com.codehub.projectfuture.webapp.enums.UserRole;
import com.codehub.projectfuture.webapp.exception.OwnersNotFoundException;
import com.codehub.projectfuture.webapp.mapper.OwnerToOwnerModelMapper;
import com.codehub.projectfuture.webapp.model.OwnerModel;
import com.codehub.projectfuture.webapp.model.RepairModel;
import com.codehub.projectfuture.webapp.repository.OwnerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class OwnerServiceImpl implements OwnerService {

    @Autowired
    private OwnerRepository ownerRepository;

    @Autowired
    private OwnerToOwnerModelMapper mapper;

    @Override
    public Owner createOwner(Owner owner) {

        return ownerRepository.save(owner);
    }

    @Override
    public void deleteById(Long id) {
        ownerRepository.deleteById(id);
    }

    @Override
    public void deleteByTaxRegistryNumber(Long taxRegistryNumber) {
        ownerRepository.deleteByTaxRegistryNumber(taxRegistryNumber);
    }

    @Override
    public Owner updateOwner(OwnerModel ownerModel) {
        Owner originalOwner = ownerRepository.findOwnerByTaxRegistryNumber(Long.parseLong(ownerModel.getTaxRegistryNumber())).get(0);
        originalOwner.setAddress(ownerModel.getAddress());
        originalOwner.setEmail(ownerModel.getEmail());
        originalOwner.setFirstName(ownerModel.getFirstName());
        originalOwner.setLastName(ownerModel.getLastName());
        originalOwner.setType(PropertyType.valueOf(ownerModel.getType()));
        originalOwner.setRole(UserRole.valueOf(ownerModel.getRole()));
        originalOwner.setPhoneNumber(ownerModel.getPhoneNumber());
        return ownerRepository.save(originalOwner);
    }

    @Override
    public List<OwnerModel> findAll() {
        return ownerRepository
                .findAll()
                .stream()
                .map(owner -> mapper.mapToOwnerModel(owner))
                .collect(Collectors.toList());
    }


    @Override
    public List<OwnerModel> findOwnerByTaxRegistryNumber(Long taxRegistryNumber) {
        return ownerRepository.findOwnerByTaxRegistryNumber(taxRegistryNumber)
                .stream()
                .map(owner -> mapper.mapToOwnerModel(owner))
                .collect(Collectors.toList());
    }

    @Override
    public Owner findByTaxRegistryNumber(Long taxRegistryNumber){
        return ownerRepository.findByTaxRegistryNumber(taxRegistryNumber);
    }

        @Override
        public List<OwnerModel> findOwnerByEmail(String email){
            return ownerRepository.findOwnerByEmail(email)
                    .stream()
                    .map(owner -> mapper.mapToOwnerModel(owner))
                    .collect(Collectors.toList());


           /* if (owners.size() == 0) {
                throw new OwnersNotFoundException("Provided Owner doesn't exist");
            }*/
        }

    @Override
    public Owner findByEmail(String email) {
        return ownerRepository.findByEmail(email);
    }


}




