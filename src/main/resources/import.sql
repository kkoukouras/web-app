-- https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-sql.html
-- https://docs.spring.io/spring-boot/docs/current/reference/html/howto-database-initialization.html
-- IN THIS FILE WE CAN WRITE AN SQL SCRIPT CONTAINING:
-- SCHEMA, TABLE AND DATA MANIPULATION QUERIES
-- TO BE EXECUTED AUTOMATICALLY DURING THE INITIALIZATION OF THE APPLICATION
-- AND AFTER THE CREATION OF SCHEMA AND TABLES BY Hibernate
-- IF spring.jpa.hibernate.ddl-auto IS SET TO create OR create-drop
-- IT IS A Hibernate feature (nothing to do with Spring)

--INSERT INTO OWNER (taxRegistryNumber, firstname, lastname, address, phonenumber, email, password, property_type) VALUES ('345689456', 'Aggelos', 'Nikolaou', 'Themistokleous 75', '694725419', 'nikolaou@yahoo.com', '12345678', 'SINGLE_APARTMENT');
INSERT INTO OWNER (taxRegistryNumber, firstname, lastname, address, phonenumber, email, password, property_type, role) VALUES ('345689456', 'Aggelos', 'Nikolaou', 'Themistokleous 75', '6947254190', 'nikolaou@yahoo.com', '$2a$10$IP2wdp.zDL0fT/fmjmjoD.T82UuLCR8ZeJR002tNHCzawNTNbgt/S', 'SINGLE_APARTMENT', 'ADMIN');
--INSERT INTO OWNER (taxRegistryNumber, firstname, lastname, address, phonenumber, email, password, property_type) VALUES ('678456908', 'Dimitris', 'Makris', 'Thermopilon 44', '6982156324', 'makris@gmail.com', '34872345', 'SINGLE_APARTMENT');
INSERT INTO OWNER (taxRegistryNumber, firstname, lastname, address, phonenumber, email, password, property_type, role) VALUES ('678456908', 'Dimitris', 'Makris', 'Thermopilon 44', '6982156324', 'makris@gmail.com', '$2a$10$LOG1tsP8CyDmNJIvNVknDevfiLBSWw50cYBgTzcVNsZM2gHbSW0Ji', 'SINGLE_APARTMENT', 'ADMIN');
--INSERT INTO OWNER (taxRegistryNumber, firstname, lastname, address, phonenumber, email, password, property_type) VALUES ('347238965', 'Vassilis', 'Mitropoulos', 'Spartis 47', '693759087', 'mitropoulos@hotmail.com', '12348244', 'BLOCK_OF_FLATS');
INSERT INTO OWNER (taxRegistryNumber, firstname, lastname, address, phonenumber, email, password, property_type, role) VALUES ('347238965', 'Vassilis', 'Mitropoulos', 'Spartis 47', '6937590870', 'mitropoulos@hotmail.com', '$2a$10$x3KDPbm4pbiBZm3OEQWH3.VxawK6bEM7.jmrcfKQ8Y4pXVtmXur62', 'BLOCK_OF_FLATS', 'USER');
--INSERT INTO OWNER (taxRegistryNumber, firstname, lastname, address, phonenumber, email, password, property_type) VALUES ('176459876', 'Thomas', 'Vasileiou', 'Troias 27', '697459867', 'vasileiou@yahoo.com', '45876234', 'COTTAGE');
INSERT INTO OWNER (taxRegistryNumber, firstname, lastname, address, phonenumber, email, password, property_type, role) VALUES ('176459876', 'Thomas', 'Vasileiou', 'Troias 27', '6974598670', 'vasileiou@yahoo.com', '$2a$10$kQPwHqs8wDXyqRudxp96wugBAqHs8rjV4yKxgl4LsNMIg025wfcIa', 'COTTAGE', 'USER');
--INSERT INTO OWNER (taxRegistryNumber, firstname, lastname, address, phonenumber, email, password, property_type) VALUES ('509834587', 'Stelios', 'Petrou', 'Eleftheriou Venizelou 134', '6936508612', 'petrou@gmail.com', '20768956', 'COTTAGE');
INSERT INTO OWNER (taxRegistryNumber, firstname, lastname, address, phonenumber, email, password, property_type, role) VALUES ('509834587', 'Stelios', 'Petrou', 'Eleftheriou Venizelou 134', '6936508612', 'petrou@gmail.com', '$2a$10$H7MloQiPuRycwKOh/cffD.TTd/L94XsKxhyIFjA2TvfQWcuBkh0Ni', 'COTTAGE', 'USER');

INSERT INTO REPAIR (datetime, status, fixType, cost, address, taxRegistryNumber, description) VALUES ('2019-08-09', 'PENDING', 'PAINT', '100', 'Eleftheriou Venizelou 134', '345689456', 'Main hall painting');
INSERT INTO REPAIR (datetime, status, fixType, cost, address, taxRegistryNumber, description) VALUES ('2019-08-27', 'PENDING', 'INSULATION', '250', 'Troias 27', '678456908', 'Ceiling insulation');
INSERT INTO REPAIR (datetime, status, fixType, cost, address, taxRegistryNumber, description) VALUES ('2019-09-01', 'ONGOING', 'HYDRAULIC_REPAIRS', '150', 'Eleftheriou Venizelou 134', '509834587', 'Sink repair');
INSERT INTO REPAIR (datetime, status, fixType, cost, address, taxRegistryNumber, description) VALUES ('2019-09-25', 'FINISHED', 'HOLLOW', '50', 'Spartis 47', '176459876', 'Fixed window hollow');
INSERT INTO REPAIR (datetime, status, fixType, cost, address, taxRegistryNumber, description) VALUES ('2019-10-08', 'ONGOING', 'ELECTRICAL_REPAIRS', '80', 'Thermopilon 44', '347238965', 'Fixing power plug');
INSERT INTO REPAIR (datetime, status, fixType, cost, address, taxRegistryNumber, description) VALUES ('2019-11-11', 'PENDING', 'HOLLOW', '50', 'Themistokleous 75', '176459876', 'Wooden door repair');
INSERT INTO REPAIR (datetime, status, fixType, cost, address, taxRegistryNumber, description) VALUES ('2019-11-18', 'FINISHED', 'PAINT', '100', 'Themistokleous 75', '345689456', 'Painted room');
INSERT INTO REPAIR (datetime, status, fixType, cost, address, taxRegistryNumber, description) VALUES ('2019-12-04', 'ONGOING', 'INSULATION', '250', 'Spartis 47', '509834587', 'Ceiling insulation');
