<#import "/spring.ftl" as spring />
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Edit Owner</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="./../css/dashboard.css">
</head>
<body>

<#include "../partials/navbar.ftlh">

<div class="container-fluid">
    <div class="row">
        <#include "../partials/sidebar_owners_create_search.ftlh">

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
            <div class="row">
                <div class="col-md-12 order-md-1">
                    <h4 class="mb-3">Edit Owner</h4>
                    <form action="/admin/owners/edit" method="post">
                        <div class="mb-3">
                            <label for="social-number">Social security number</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="social-number" value="${owner.taxRegistryNumber}" name="taxRegistryNumber" required>
                                <div class="invalid-feedback" style="width: 100%;">
                                    Social number is required.
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="first-name">First name</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="first-name" name="firstName" value="${owner.firstName}" required>
                                    <div class="invalid-feedback">
                                        Valid first name is required.
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="last-name">Last name</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="last-name" name="lastName" value="${owner.lastName}" required>
                                    <div class="invalid-feedback">
                                        Valid last name is required.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="address">Address</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="address" name="address" value="${owner.address}" required>
                                <div class="invalid-feedback">
                                    Please enter an address.
                                </div>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="phone">Phone number</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="phone" name="phoneNumber" value="${owner.phoneNumber}" required>
                                <div class="invalid-feedback" style="width: 100%;">
                                    Phone number is required.
                                </div>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="email">Email</label>
                            <div class="input-group">
                                <input type="email" class="form-control" id="email" name="email" value="${owner.email}" required>
                                <div class="invalid-feedback">
                                    Please enter a valid email address.
                                </div>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="password">Password</label>
                            <div class="input-group">
                                <input type="password" class="form-control" id="password" name="password" value="hashed" required>
                                <div class="invalid-feedback">
                                    Please enter a valid password.
                                </div>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="type">Property Type</label>
                            <div class="input-group">
                                <select class="custom-select d-block w-100" id="type" name="type" required>
                                    <#if propertyType??>
                                        <#list propertyType as type>
                                            <option value="${type}"<#if owner.type == type> selected</#if>>${type}</option>
                                        </#list>
                                    </#if>
                                </select>
                                <div class="invalid-feedback">
                                    Please select a property type.
                                </div>
                            </div>
                        </div>

                        <div class="mb-3">
                            <label for="role">Owner's Role</label>
                            <div class="input-group">
                                <select class="custom-select d-block w-100" id="role" name="role" required>
                                    <#if ownersRole??>
                                        <#list ownersRole as role>
                                            <option value="${role}"<#if owner.role == role> selected</#if>>${role}</option>
                                        </#list>
                                    </#if>
                                </select>
                                <div class="invalid-feedback">
                                    Please select an Owner's Role.
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-primary btn-lg mt-4" type="submit">Update</button>
                    </form>
                </div>
            </div>
        </main>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script>window.jQuery || document.write('<script src="./../js/vendor/jquery-slim.min.js"><\/script>');</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
<script>
    feather.replace();
</script>
<script src="/scripts/validation.js"></script>
</body>
</html>